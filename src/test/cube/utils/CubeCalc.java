package test.cube.utils;


import test.cube.model.Coordinate;
import test.cube.model.Cube;
import test.cube.model.Face;

public class CubeCalc {
	
	public static Face calcBackFace(Coordinate center,double centerDistance){
		Coordinate top_left=new Coordinate();
		top_left.setX(center.getX()-centerDistance);
		top_left.setY(center.getY()+centerDistance);
		top_left.setZ(center.getZ()+centerDistance);
		
		Coordinate top_right= new Coordinate();
		top_right.setX(center.getX()+centerDistance);
		top_right.setY(center.getY()+centerDistance);
		top_right.setZ(center.getZ()+centerDistance);
		
		Coordinate bottom_left= new Coordinate();
		bottom_left.setX(center.getX()-centerDistance);
		bottom_left.setY(center.getY()-centerDistance);
		bottom_left.setZ(center.getZ()+centerDistance);
		
		Coordinate bottom_right= new Coordinate();
		bottom_right.setX(center.getX()+centerDistance);
		bottom_right.setY(center.getY()-centerDistance);
		bottom_right.setZ(center.getZ()+centerDistance);
		return new Face(top_left, top_right, bottom_left, bottom_right);
	}
	public static Face calcFrontFace(Coordinate center,double centerDistance){
		Coordinate top_left= new Coordinate();
		top_left.setX(center.getX()-centerDistance);
		top_left.setY(center.getY()+centerDistance);
		top_left.setZ(center.getZ()-centerDistance);
		
		Coordinate top_right= new Coordinate();
		top_right.setX(center.getX()+centerDistance);
		top_right.setY(center.getY()+centerDistance);
		top_right.setZ(center.getZ()-centerDistance);
		
		Coordinate bottom_left= new Coordinate();
		bottom_left.setX(center.getX()-centerDistance);
		bottom_left.setY(center.getY()-centerDistance);
		bottom_left.setZ(center.getZ()-centerDistance);
		
		Coordinate bottom_right= new Coordinate();
		bottom_right.setX(center.getX()+centerDistance);
		bottom_right.setY(center.getY()-centerDistance);
		bottom_right.setZ(center.getZ()-centerDistance);
		return new Face(top_left, top_right, bottom_left, bottom_right);
	}

	public static void verifyCubes(Cube a, Cube b) {
		System.out.println("-----------------------------------------------------------------------");
		boolean[] result= intersect(a, b);
		if(result[0] && result[1]){
			System.out.println("Do the cubes intersect? Yes");
			System.out.println("What is the volume of the shared space? " + b.getVolume());
		}else if(!result[0] && !result[1]){
			boolean[] result2= intersect(b, a);
			if(result2[0] && result2[1]){
				System.out.println("Do the cubes intersect? Yes");
				System.out.println("What is the volume of the shared space? " + a.getVolume());
			}else if(!result2[0] && !result2[1]){
				System.out.println("Do the cubes intersect? No");
				System.out.println("What is the volume of the shared space? 0");
			}else if(result2[0] && !result2[1]){
				System.out.println("Do the cubes intersect? Yes");
				System.out.println("What is the volume of the shared space? Checking...");
				calcSharedVolume(b, a);
			}
		}else if(result[0] && !result[1]){
			System.out.println("Do the cubes intersect? Yes");
			System.out.println("What is the volume of the shared space? Checking...");
			calcSharedVolume(a, b);
		}
		System.out.println("-----------------------------------------------------------------------");
		
		
	}
	
	/**
	 * Validates the intersection with the vertices
	 * @param a
	 * @param b
	 * @return  [true, true] if intersects and the shape is 100%
	 * 			[true, false] if intersects
	 * 			[false, false] if not intersects
	 */
	private static boolean[] intersect(Cube a, Cube b){

		int[] result = checkVerticeAll(a, b);
		//if all the vertices are inside the cube a
		boolean inside=true;
		for(int tmpResult: result){
			if(tmpResult!=1){
				inside=false;
				break;
			}
		}
		if(inside)
			return new boolean[]{true,true};
			
		//if all the vertices are outside the cube a
		boolean outside=true;
		int sum=0;
		for(int tmpResult: result){
			sum+=tmpResult;
			if(!(tmpResult<=0)){
				outside=false;
				break;
			}
		}
		if(outside){
			//if the sum of the results is 0, means all the vertix are the same, so the shape is 100%, ortrhewise its outside
			if(sum<0){
				return new boolean[]{false,false};
			}else{
				return new boolean[]{true,true};
			}
		
		}
		
		//if all the vertices intersect, means that its inside in some edge
		boolean intersect=true;
		for(int tmpResult: result){
			if(!(tmpResult>=0)){
				intersect=false;
				break;
			}
		}
		if(intersect){
			return new boolean[]{true,true};
		//if none of the cases before fits, means that they intersect and need the % calc of the shape
		}else{
			for(int tmpResult: result){
				System.out.print(tmpResult+",");
			}
			System.out.println();
			return new boolean[]{true,false};
		}
	}

	/**
	 * validates de vertix is inside the cube or at the same position
	 * @param a cube
	 * @param vertix to check
	 * @return -1 no match, 0 the same vertix, 1 inside
	 */
	private static int checkVertice(Cube a, Coordinate vertix){
		Face frontFace = a.getFrontFace();
		Face backFace = a.getBackFace();
		if(frontFace.getTop_left().getX()<vertix.getX() && frontFace.getTop_right().getX()>vertix.getX()
				&& frontFace.getTop_left().getY()>vertix.getY() && frontFace.getBottom_left().getY()<vertix.getY()
				&& frontFace.getTop_left().getZ()<vertix.getZ() && backFace.getTop_left().getZ()>vertix.getZ()){
			return 1;
		}else if(frontFace.getTop_left().getX()<=vertix.getX() && frontFace.getTop_right().getX()>=vertix.getX()
				&& frontFace.getTop_left().getY()>=vertix.getY() && frontFace.getBottom_left().getY()<=vertix.getY()
				&& frontFace.getTop_left().getZ()<=vertix.getZ() && backFace.getTop_left().getZ()>=vertix.getZ()){
			return 0;
			
		}else{
			return -1;
		}
	}
	
	private static int[] checkVerticeAll(Cube a, Cube b){
		int resultTop_left=checkVertice(a, b.getFrontFace().getTop_left());
		int resultTop_right=checkVertice(a, b.getFrontFace().getTop_right());
		int resultTop_left_back=checkVertice(a, b.getBackFace().getTop_left());
		int resultTop_right_back=checkVertice(a, b.getBackFace().getTop_right());
		int resultBottom_left=checkVertice(a, b.getFrontFace().getBottom_left());
		int resultBottom_right=checkVertice(a, b.getFrontFace().getBottom_right());
		int resultBottom_left_back=checkVertice(a, b.getBackFace().getBottom_left());
		int resultBottom_right_back=checkVertice(a, b.getBackFace().getBottom_right());
		int[] result = new int[]{resultTop_left,resultTop_right,resultTop_left_back,resultTop_right_back,
				resultBottom_left,resultBottom_right,resultBottom_left_back,resultBottom_right_back};
		return result;
	}
	
	private static void calcSharedVolume(Cube a, Cube b){
		double differenceX=0;
		double differenceY=0;
		double differenceZ=0;
		Face frontFace = a.getFrontFace();
		Face backFace = a.getBackFace();

		//verify if the edge X is inside the cube
		if(frontFace.getTop_left().getX()<b.getFrontFace().getTop_left().getX() 
				&& frontFace.getTop_right().getX()>b.getFrontFace().getTop_right().getX()){
			//if its inside take entire
			differenceX=b.getFrontFace().getTop_right().getX()-b.getFrontFace().getTop_left().getX();
		// if not, check which side is out of range
		}else{
			if(frontFace.getTop_left().getX()<b.getFrontFace().getTop_left().getX()){
				differenceX=frontFace.getTop_left().getX()-b.getFrontFace().getTop_left().getX();
			}else{
				differenceX=frontFace.getTop_right().getX()-b.getFrontFace().getTop_right().getX();
			}
		}
		//verify if the edge Y is inside the cube
		if(b.getFrontFace().getTop_left().getY()<frontFace.getTop_left().getY() 
				&& b.getFrontFace().getBottom_left().getY()>frontFace.getBottom_left().getY()){
			differenceY=b.getFrontFace().getTop_left().getY()-b.getFrontFace().getBottom_left().getY();
		// if not, check which side is out of range
		}else{
			if(b.getFrontFace().getTop_left().getY()<frontFace.getTop_left().getY() ){
				differenceY=b.getFrontFace().getTop_left().getY()-frontFace.getTop_left().getY() ;
			}else{
				differenceY=b.getFrontFace().getBottom_left().getY()-frontFace.getBottom_left().getY();
			}
		}
		//verify if the edge Z is inside the cube
		if(frontFace.getTop_left().getZ()<b.getFrontFace().getTop_left().getZ() 
				&& backFace.getTop_left().getZ()>b.getBackFace().getTop_left().getZ()){
			differenceZ=b.getBackFace().getTop_left().getZ()-b.getFrontFace().getTop_left().getZ();
		//if not, check which side is out of range
		}else{
			if(frontFace.getTop_left().getZ()<b.getFrontFace().getTop_left().getZ()){
				differenceZ=b.getFrontFace().getTop_left().getZ()+frontFace.getTop_left().getZ();
			}else{
				differenceZ=b.getBackFace().getTop_left().getZ()-backFace.getTop_left().getZ();
			}
		}
//		System.out.println(String.format("differenceX: "+differenceX+" differenceY: "+differenceY+" differenceZ: "+differenceZ));
		System.out.println("Shared volume: "+(Math.abs(differenceX) * Math.abs(differenceY) * Math.abs(differenceZ)));
	}
	

}
