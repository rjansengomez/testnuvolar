package test.cube.validation;

import test.cube.model.Coordinate;

public class CubeValidator {
	
	public static void validateInputData(double size, Coordinate center) throws Exception{
		if( size==0){
			throw new Exception("Invalid size: " + size);
		}
		if(center==null){
			throw new Exception("Invalid coordinate: null");
		}
	}

}
