package test.cube.model;


public class Face {

	private Coordinate top_left;
	private Coordinate top_right;
	private Coordinate bottom_left;
	private Coordinate bottom_right;
	
	public Face(Coordinate top_left, Coordinate top_right, Coordinate bottom_left, Coordinate bottom_right){
		this.setTop_left(top_left);
		this.setTop_right(top_right);
		this.setBottom_left(bottom_left);
		this.setBottom_right(bottom_right);
	}

	public Coordinate getTop_left() {
		return top_left;
	}

	public void setTop_left(Coordinate top_left) {
		this.top_left = top_left;
	}

	public Coordinate getTop_right() {
		return top_right;
	}

	public void setTop_right(Coordinate top_right) {
		this.top_right = top_right;
	}

	public Coordinate getBottom_left() {
		return bottom_left;
	}

	public void setBottom_left(Coordinate bottom_left) {
		this.bottom_left = bottom_left;
	}

	public Coordinate getBottom_right() {
		return bottom_right;
	}

	public void setBottom_right(Coordinate bottom_right) {
		this.bottom_right = bottom_right;
	}

	@Override
	public String toString() {
		return "Face [top_left=" + top_left + ", top_right=" + top_right + ", bottom_left=" + bottom_left
				+ ", bottom_right=" + bottom_right + "]";
	}
	
	
}
