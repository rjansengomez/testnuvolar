package test.cube.model;

import test.cube.utils.CubeCalc;
import test.cube.validation.CubeValidator;

/**
 * Model container of a cube data
 * @author Ram�n
 *
 */
public class Cube{
	private double size;
	private Coordinate center;
	private Face backFace;
	private Face frontFace;
	private double centerDistance;
	private double volume;
	
	public Cube(double size,Coordinate centerPoint) throws Exception{
		CubeValidator.validateInputData(size,centerPoint);
		this.size=size;
		this.center=centerPoint;
		this.centerDistance = size/2;
		this.volume=Math.pow(size, 3);
		this.setBackFace(CubeCalc.calcBackFace(this.center, this.centerDistance));
		this.setFrontFace(CubeCalc.calcFrontFace(this.center, this.centerDistance));
	}
	
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
			
	public Face getFrontFace() {
		return frontFace;
	}

	public void setFrontFace(Face frontFace) {
		this.frontFace = frontFace;
	}

	public Face getBackFace() {
		return backFace;
	}

	public void setBackFace(Face backFace) {
		this.backFace = backFace;
	}
	
	public Coordinate[][][] fillCube(){
		int tmp2 = (int)size; 
		Coordinate[][][] tmp =new Coordinate[tmp2][tmp2][tmp2];
		return tmp;
	}

	@Override
	public String toString() {
		return "Cube [size=" + size + ", center=" + center + ", centerDistance=" + centerDistance 
				+  ", volume=" + volume + ", backFace=" + backFace + ", frontFace=" + frontFace+ "]";
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}
	
	public Coordinate getCenter(){
		return this.center;
	}
	
	
}
