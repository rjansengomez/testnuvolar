package test.cube.model;


public class Coordinate {
	private double z;
	private double y;
	private double x;
	
	public double getZ() {
		return z;
	}
	public void setZ(double z) {
		this.z = z;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	
	@Override
	public String toString() {
		return "Coordinate [z=" + z + ", y=" + y + ", x=" + x + "]";
	}
	@Override
	public boolean equals(Object obj){
		if(obj instanceof Coordinate){
			Coordinate aCoordinate = (Coordinate) obj;
			return aCoordinate.getX()== this.x && aCoordinate.getY() == this.y && aCoordinate.getZ() == this.z; 
		}
		return false;
	}
	
	
}
