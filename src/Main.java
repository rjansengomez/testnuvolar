import org.junit.Before;
import org.junit.Test;

import test.cube.model.Coordinate;
import test.cube.model.Cube;
import test.cube.utils.CubeCalc;

public class Main {
	private Cube a;
	private Cube b;
	private Coordinate centerPointA;
	private Coordinate centerPointB;

	
	@Before 
	public void initMocks() throws Exception {
		setCubeTest();
	}
	
	@Test
	public void testCubeEntry(){
		System.out.println("Test 1");
		CubeCalc.verifyCubes(a,b);
	}

	@Test
	public void testCubeEntry2() throws Exception{
		System.out.println("Test 2");
		centerPointA.setZ(17);
		a= new Cube(5,centerPointA);
		CubeCalc.verifyCubes(a,b);
	}
	
	@Test
	public void testCubeEntry3() throws Exception{
		System.out.println("Test 3");
		Coordinate centerB = new Coordinate();
		centerB.setX(10);
		centerB.setY(10);
		centerB.setZ(0);
		b= new Cube(6,centerB);
		CubeCalc.verifyCubes(a,b);
	}
	@Test
	public void testCubeEntry4() throws Exception{
		centerPointB = new Coordinate();
		centerPointB.setX(9);
		centerPointB.setY(9);
		centerPointB.setZ(0);
		
		b= new Cube(6,centerPointB);
		System.out.println("Test 4");
		CubeCalc.verifyCubes(a,b);
	}
	
	@Test
	public void testCubeEntry5() throws Exception{
		centerPointB = new Coordinate();
		centerPointB.setX(9);
		centerPointB.setY(9);
		centerPointB.setZ(3);
		
		b= new Cube(2,centerPointB);
		System.out.println("Test 5");
		CubeCalc.verifyCubes(a,b);
	}
	
	@Test
	public void testCubeEntry6() throws Exception{
		centerPointA = new Coordinate();
		centerPointA.setX(-10);
		centerPointA.setY(-10);
		centerPointA.setZ(0);
		centerPointB = new Coordinate();
		centerPointB.setX(-9);
		centerPointB.setY(-9);
		centerPointB.setZ(0);
		
		a= new Cube(5,centerPointA);
		b= new Cube(2,centerPointB);
		System.out.println("Test 6");
		CubeCalc.verifyCubes(a,b);
	}
	
	@Test
	public void testCubeEntry7() throws Exception{
		centerPointA = new Coordinate();
		centerPointA.setX(-10);
		centerPointA.setY(-10);
		centerPointA.setZ(0);
		centerPointB = new Coordinate();
		centerPointB.setX(-9);
		centerPointB.setY(-9);
		centerPointB.setZ(3);
		
		a= new Cube(5,centerPointA);
		b= new Cube(2,centerPointB);
		System.out.println("Test 7");
		CubeCalc.verifyCubes(a,b);
	}
	
	
	private void setCubeTest() throws Exception{

		centerPointA = new Coordinate();
		centerPointA.setX(10);
		centerPointA.setY(10);
		centerPointA.setZ(0);
		centerPointB = new Coordinate();
		centerPointB.setX(9);
		centerPointB.setY(9);
		centerPointB.setZ(0);
		
		a= new Cube(5,centerPointA);
		b= new Cube(2,centerPointB);
		
	}
	

}
